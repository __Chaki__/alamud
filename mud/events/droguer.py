from .event import Event2, Event3

class DroguerEvent(Event2):
    NAME = "droguer"

    def perform(self):
        if self.object.has_prop("droguable"):
            return self.informe("droguer.failed")
        self.inform("drogue")
