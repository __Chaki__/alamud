from .action     import Action2
from mud.events import DroguerEvent


class DroguerAction(Action2):
    EVENT = DroguerEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "drogue"
